/* global eth, miner, admin, net */

// For Ethereum Networks with light activity
// automatically enable and disbale miner
//
// set var `thread` in global geth instance to change CPUs
//
// loadScript('eth-smart-mine.js')
//
// adhered to standardjs.com/
var threads = 1
// fallback for for networkid 1429 only
var defaultBase = eth.accounts[0] || '0xbcf647384f0d9fac697264449fe3baf1508e5350'

function checkWork (_err, _block) {
  if (_err) {
    console.log('ERROR xx ')
    return
  }
  console.log('+ ~ + ~ + + ~ + ~ + + ~ + ~ + + ~ + ~ + ~ + + ~ + ~ + + ~ + ~ +')
  console.log('Block          ', JSON.stringify(_block))
  console.log('Pending Transactions', eth.pendingTransactions.length)

  if (eth.pendingTransactions.length > 0) {
    if (eth.mining) return
    console.log('== Pending transactions! Mining. Threads: ', threads)
    miner.start(threads)
  } else {
    miner.stop()
    console.log('== No transactions! Mining stopped.')
  }
  console.log('+ ~ + ~ + + ~ + ~ + + ~ + ~ + + ~ + ~ + ~ + + ~ + ~ + + ~ + ~ +')
}

eth.filter('latest', function (err, block) {
  checkWork(err, block)
})
eth.filter('pending', function (err, block) {
  checkWork(err, block)
})

// check for base to be set!
if (!eth.coinbase) {
  miner.setEtherbase(defaultBase)
}

if (net.listening) {
  console.log('+ ~ + ~ + + ~ + ~ + + ~ + ~ + + ~ + ~ + ~ + + ~ + ~ + + ~ + ~ +')
  console.log('* Smart Mining Active *')
  console.log('+ ~ + ~ + + ~ + ~ + + ~ + ~ + + ~ + ~ + ~ + + ~ + ~ + + ~ + ~ +')
  console.log('Etherbase      ', eth.coinbase)
  console.log('CPU Threads    ', threads)
  console.log('Network Id     ', net.version)
  console.log('Peers          ', admin.peers.length)
  console.log('+ ~ + ~ + + ~ + ~ + + ~ + ~ + + ~ + ~ + ~ + + ~ + ~ + + ~ + ~ +')
} else {
  console.log(net)
  console.log('You are not connected to an Ethereum JSON-RPC')
}
